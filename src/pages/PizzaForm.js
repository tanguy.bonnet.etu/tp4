import Page from './Page.js';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = this.element.querySelector('.pizzaForm');
		form.addEventListener('submit', event => {
			event.preventDefault();
			this.submit();
		});
	}

	submit() {
		// D.4. La validation de la saisie
		const nameInput = this.element.querySelector('input[name="name"]'),
			name = nameInput.value;
		const imageInput = this.element.querySelector('input[image="image"]'),
			image = nameInput.value;
		const smallInput = this.element.querySelector(
				'input[priceSmall="priceSmall"]'
			),
			priceSmall = nameInput.value;
		const LargeInput = this.element.querySelector(
				'input[priceLarge="priceLarge"]'
			),
			priceLarge = nameInput.value;
		if (name === '') {
			alert('Erreur : le champ "Nom" est obligatoire');
			return;
		}
		if (image === '') {
			alert('Erreur : le champ "Image" est obligatoire');
			return;
		}
		if (priceSmall === '') {
			alert('Erreur : le champ "priceSmall" est obligatoire');
			return;
		}
		if (priceLarge === '') {
			alert('Erreur : le champ "priceLarge" est obligatoire');
			return;
		}
		const pizza = {
			name,
			image,
			priceSmall,
			priceLarge,
		};
		fetch('http://localhost:8080/api/v1/pizzas', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(pizza),
		});
		alert(`La pizza ${name} a été ajoutée !`);
		nameInput.value = '';
	}
}
